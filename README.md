Instrucciones de la línea de comando  =>  Configuración Repositorio de GitLab

También puede subir archivos existentes desde su ordenador utilizando las instrucciones que se muestran a continuación. <br>
Configuración global de Git  <br>

git config --global user.name "UserName"  <br>
git config --global user.email "useremail@mail.com"  <br>

Crear un nuevo repositorio

git clone https://gitlab.com/proyecto-simbu/appsimbu.git  <br>
cd appsimbu  <br>
touch README.md  <br>
git add README.md  <br>
git commit -m "add README"  <br>
git push -u origin master  <br>

Push a una carpeta existente

cd existing_folder  <br>
git init  <br>
git remote add origin https://gitlab.com/proyecto-simbu/appsimbu.git  <br>
git add .  <br>
git commit -m "Initial commit"  <br>
git push -u origin master  <br>

Push a un repositorio de Git existente

cd existing_repo  <br>
git remote rename origin old-origin  <br>
git remote add origin https://gitlab.com/proyecto-simbu/appsimbu.git  <br>
git push -u origin --all  <br>
git push -u origin --tags  <br>




