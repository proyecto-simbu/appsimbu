import {Injectable} from '@angular/core';
import{AngularFirestore, AngularFirestoreDocument,
AngularFirestoreCollection} from '@angular/fire/firestore';

@Injectable({
    providedIn: 'root'
})

export class BaseDedatoService{
    constructor(
        public FireStore: AngularFirestore){}
    
        crearDocument<tipo>(data: tipo, enlace: string){
            const itemsCollection: AngularFirestoreCollection<tipo>=
                this.FireStore.collection<tipo>(enlace);
                return itemsCollection.add(data);
        }

        deleteDocument(){

        }

        getDocument(){

        }
        editDocument(){

        }
}
