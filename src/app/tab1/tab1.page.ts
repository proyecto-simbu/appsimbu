import { Component } from '@angular/core';

import {BluetoothSerial} from '@ionic-native/bluetooth-serial/ngx';
import {AlertController} from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
    Divices
  constructor(private bluetoothSeral:BluetoothSerial, private alertController:AlertController) {}

  ActivarBluetooth(){
    this.bluetoothSeral.isEnabled().then(response=>{
      console.log("isOn");
    },error=>{
      console.log("isOff")
    })
  }
    Listdivices(){
      this.bluetoothSeral.list().then(response=>{
        this.Divices=response

      },error => {
        console.log("error")
      })
    }


    connect(address){
      this.bluetoothSeral.connect(address).subscribe(success=>{
        this.deviceConnected()
      }, error=>{
        console.log("error")
      })
      
    }
    
    Disconected(){
      this.bluetoothSeral.disconnect()
      console.log("dispositivo desconectado")
    }

    deviceConnected(){
      this.bluetoothSeral.subscribe('\n').subscribe(success=>{
        this.hundler(success)
      })
    }

    hundler(value){
      console.log(value)
    }
      

  async isEnabled(msg){
    const alert= this.alertController.create({
      header:'Alerta',
      message:msg,
      buttons:[{
        text:'Okay',
        handler: ()=>{
          console.log("okay")
        }
      } ]
    })
  }


}
