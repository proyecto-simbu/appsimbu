// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig: {
    apiKey: "AIzaSyA8YMH5UbgrK3FATUekPv5VBPvXTjFCPYM",
    authDomain: "simbu-35849.firebaseapp.com",
    projectId: "simbu-35849",
    storageBucket: "simbu-35849.appspot.com",
    messagingSenderId: "221049704135",
    appId: "1:221049704135:web:6cbb889df28ffcae72f8e2",
    measurementId: "G-8YQGRG0657"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
